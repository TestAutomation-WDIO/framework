import {expect} from 'chai';
import { Then } from 'cucumber';



Then('should the title of the page be {string}' , expectedTitle => {
    const title = browser.getTitle();
    expect(title).to.equal(expectedTitle);

})