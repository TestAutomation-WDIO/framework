Installation:
This framework is WebdriverIO cucumber framework with page object model implemented and babel as JavaScript compiler.
Clone the framework from bitbucket and run npm install to install all the required dependencies listed in package.json file.
To Write the tests: 
Write the features in features folder and write the required step definitions for the feature in given ,then and when files.

To Run: 
On Production:
wdio wdio.conf.js

On staging:
Wdio.staging.conf.js

Folder structure:
 

Src contains all the folders related to features, Page Objects and Step Definitions.
Logs, Reports and screenshots will be captured in respective folder.
Please Refer the wdio.conf.js to know more about the settings of the framework. These can be changed as per requirements in config file.
Note: Wendriverio v4 is used for this framework as cucumber is not yet supported in v5. Once cucumber is available in v5 this framework can be upgraded.
WebDriverIO Framework: 
Please refer webdriverio api documentation to more about the framework , they have excellent documentation on each and every topic . 
https://webdriver.io/docs/api.html

