var merge = require('deepmerge');
var wdioConf = require('./wdio.conf.js');
exports.config = merge(wdioConf.config, {
    capabilities: [
        // more caps defined here
        // ...
    ],
    
    baseUrl:'https://staging.www.aplaceformom.com',
 }, { clone: false });